//Polina Yankovich 1834306 02-09-2022
package vehicles;
public class Bicycle{
    private String manufacturer;
    private int numberGears;
    private double maxSpeed;
//constructor for Bicycle 
public Bicycle(String manufacturer, int numberGears, double maxSpeed ){
this.manufacturer=manufacturer;
this.numberGears=numberGears;
this.maxSpeed=maxSpeed;
}
    //GET methods for class Bicycle
public void getManufacturer(){
    System.out.println(this.manufacturer);
}
public int getNumberGears(){
    return this.numberGears;
}
public double getMaxSpeed(){
    return this.maxSpeed;
}

//to String method
public String toString(){
    return "Manufacturer: "+ this.manufacturer+ '\n' +  "Number of Gears: "+ this.numberGears +'\n'+ "MaxSpeed: " +this.maxSpeed; 
}
}