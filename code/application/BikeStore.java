//Polina Yankovich 1834306 02-09-2022
package application;
import vehicles.Bicycle;
public class BikeStore {
    public static void main (String[] args){
Bicycle[] bikes =new Bicycle[4];
//bike values
bikes[0]= new Bicycle("Bixii",21,22);
bikes[1]= new Bicycle("TheFastOne",25,50);
bikes[2]= new Bicycle("Light",15,35);
bikes[3]= new Bicycle("Zoop",19,45);
//print all bike types 
for (Bicycle bike : bikes){
    System.out.println(bike);
    System.out.println('\n');
    }
}
}
